<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Pegawai extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    }

    public function indexPegawai_get()
    {
        $data = $this->db->query("SELECT * FROM pegawai ")->result();
        $this->response([
            'status' => TRUE,
            'message' => 'Berhasil',
            'data'    => $data
        ], REST_Controller::HTTP_OK);
    }

    public function tambahPegawai_post()
    {
        $nama           = $this->input->post('nama');
        $nip            = $this->input->post('nip');
        $tempat_lahir   = $this->input->post('tempat_lahir');
        $tanggal_lahir  = $this->input->post('tanggal_lahir');

        $data = array(
            'nama'          => $nama,
            'nip'           => $nip,
            'tempat_lahir'  => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir
        );

        $query = $this->db->insert('pegawai', $data);
        if ($query) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil Tambah Data Pegawai',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gagal Tambah Data Pegawai',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        }
    }

    public function detailPegawai_post()
    {
        $id    = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM pegawai WHERE id = '$id'");
        if ($query->num_rows == 1) {
            $data = $query->row();
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil Menampilkan Detail Pegawain',
                'data'    => $data
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gagal Menampilkan Detail Pegawain',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        }
    }

    public function editPegawai_post()
    {
        $id             = $this->input->post('id');
        $nama           = $this->input->post('nama');
        $nip            = $this->input->post('nip');
        $tempat_lahir   = $this->input->post('tempat_lahir');
        $tanggal_lahir  = $this->input->post('tanggal_lahir');

        $data = array(
            'nama'          => $nama,
            'nip'           => $nip,
            'tempat_lahir'  => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir
        );

        $this->db->where('id', $id);
        $query = $this->db->update('pegawai', $data);
        if ($query) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil Edit Data Pegawai',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gagal Edit Data Pegawai',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        }
    }

    public function hapusPegawai_post()
    {
        $id             = $this->input->post('id');
        $this->db->where('id', $id);
        $query = $this->db->delete('pegawai');
        if ($query) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil Edit Data Pegawai',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gagal Edit Data Pegawai',
                'data'    => array(),
            ], REST_Controller::HTTP_OK);
        }
    }
}
